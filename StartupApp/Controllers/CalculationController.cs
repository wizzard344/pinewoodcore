﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StartupApp.Attributes;
using StartupApp.Filters;

namespace StartupApp.Controllers
{
    public class CalculationController : Controller
    {
        [FilterFactory(typeof(CalculationFilter))]
        public string Index()
        {
            var data = 0;
            var calculationResult = HttpContext.Items.FirstOrDefault(e => e.Key.ToString() == "result").Value;
            return data.ToString();
        }
    }
}
