﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StartupApp.Services;

namespace StartupApp.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FilterFactoryAttribute : Attribute, IFilterFactory
    {
        public string Identifier { get; set; }

        public bool IsReusable => false;

        private Type FilterType { get; }

        public FilterFactoryAttribute(Type filterType)
        {
            FilterType = filterType;
        }

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            return serviceProvider.GetService(FilterType) as IFilterMetadata;
        }
    }
}
