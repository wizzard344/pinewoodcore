﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using StartupApp.Services;

namespace StartupApp.Filters
{
    public class CalculationFilter : ActionFilterAttribute
    {
        private readonly CalculationService _calculationService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CalculationFilter(CalculationService calculationService, IHttpContextAccessor httpContextAccessor)
        {
            _calculationService = calculationService;
            _httpContextAccessor = httpContextAccessor;
        }


        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var result = _calculationService.Calculate().ToString();
            context.HttpContext.Response.Headers.Add("X-Shit", result);

            context.HttpContext.Items.Add("result", result);

            await base.OnActionExecutionAsync(context, next);
        }

        private bool IsNameAdmin(ActionExecutingContext context)
        {
            return string.Equals(context.Controller.GetType().Name, "Admin", StringComparison.OrdinalIgnoreCase);
        }
    }
}
