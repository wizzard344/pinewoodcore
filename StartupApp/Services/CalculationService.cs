﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StartupApp.Services
{
    public class CalculationService
    {
        public CalculationService()
        {

        }

        public int Calculate()
        {
            Random rng = new Random();
            return 1 + rng.Next(int.MaxValue);
        }
    }
}
