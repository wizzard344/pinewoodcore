﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace StartupApp.Services
{
    public class RandomWebRootProvider: IFileProvider
    {
        private Random rng = new Random();
        private string[] sites = new string[] { "site1", "site2" };
        private IHostingEnvironment _env;
        private IHttpContextAccessor _httpAccessor;
        Dictionary<string, IFileProvider> _providers = new Dictionary<string, IFileProvider>();
        public RandomWebRootProvider(IHostingEnvironment env, IHttpContextAccessor httpAccessor)
        {
            _httpAccessor = httpAccessor;
            _env = env;
            foreach (var item in sites)
            {
                _providers.Add(item, new PhysicalFileProvider(Path.Combine(_env.WebRootPath, item)));
            }
        }

        public IDirectoryContents GetDirectoryContents(string subpath)
        {
            var provider = new PhysicalFileProvider(Path.Combine(_env.WebRootPath, sites[rng.Next(0, 1)]));
            return provider.GetDirectoryContents(subpath);
        }

        public IFileInfo GetFileInfo1(string subpath)
        {
            //var http = _httpAccessor.HttpContext.Request.Host;
            var sw = Stopwatch.StartNew();
            var rand = sites[rng.Next(0, 2)];
            var provider = new PhysicalFileProvider(Path.Combine(_env.WebRootPath, rand));
            var result = provider.GetFileInfo(subpath);
            sw.Stop();
            var x = sw.Elapsed.TotalMilliseconds;
            return result;
        }

        public IFileInfo GetFileInfo(string subpath)
        {
            //var http = _httpAccessor.HttpContext.Request.Host;
            var sw = Stopwatch.StartNew();
            var rand = sites[rng.Next(0, 2)];
            var result = _providers[rand].GetFileInfo(subpath);
            sw.Stop();
            var x = sw.Elapsed.TotalMilliseconds;
            return result;
        }

        public IChangeToken Watch(string filter)
        {
            // ???????
            throw new NotImplementedException();
        }
    }
}
